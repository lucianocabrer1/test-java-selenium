package com.overactive.selenium;

import java.util.List;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class App 
{
	public static WebDriver driver = null;
    public static void main( String[] args )
    {
     //   System.out.println( "Hello World!" );
    }
    
    public WebDriver inicializar(String tpoDriver, String ruta) {
    	
    	tpoDriver = tpoDriver.toLowerCase();
    	
    	if(tpoDriver == "firefox") {
    		
    		tpoDriver = "webdriver.gecko.driver";
    		
    		System.setProperty(tpoDriver, ruta);
    		
    		try {
    			
    			driver = new FirefoxDriver();
    			
    		}catch(Exception e) {
    			
    			System.out.println("Ha fallado el driver al inicializar por: " + e);
    			
    		}
    		
    	}else if(tpoDriver == "chrome") {

    		tpoDriver = "webdriver." + tpoDriver +".driver";
    		
    		System.setProperty(tpoDriver, ruta);
    		
    		try {
    			
    			driver = new ChromeDriver();
    		}catch(Exception e) {
    		
    			System.out.println("Ha fallado el driver al inicializar por: " + e);
    		
    		}
    		
    	}
    	
    	return driver;
    }
    
    public void instanciarPagina(WebDriver driver, String webPage) {
    	
    	driver.get(webPage);
    
    }
    
    public void cerrarNavegador(WebDriver driver) {
    	
    	try {
    	
    		driver.close();
    		
    	}catch(Exception e) {
    		
    		System.out.println("Ha fallado al cerrar el navegador por: " + e);
    	
    		System.exit(1);
    	}finally {
    		
    		System.exit(0);
    		
    	}
    	
    }
    
    public void selectOptionWithText(WebDriver driver, long timeOut,
    								 String textToSelect) {
		try {
			WebElement autoOptions = driver.findElement(By.id("ui-id-8"));
			
			WebDriverWait wait = new WebDriverWait(driver, timeOut);
			
			wait.until(ExpectedConditions.visibilityOf(autoOptions));

			List<WebElement> optionsToSelect = autoOptions.findElements(By.tagName("li"));
			
			for(WebElement option : optionsToSelect){
		    
				if(option.getText().equals(textToSelect)) {
		        
					System.out.println("Trying to select: "+textToSelect);
		            
					option.click();
		            
					break;
		        }
		    }
			
		} catch (NoSuchElementException e) {
			System.out.println(e.getStackTrace());
		}
		catch (Exception e) {
			System.out.println(e.getStackTrace());
		}
	}
    
}
