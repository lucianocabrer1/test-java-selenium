package com.overactive.selenium;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.regex.Pattern;
import java.util.*;

public class LeerConScanner{
	
	public static void main(String[] args) throws FileNotFoundException {
		
		//obtenerLista();
	}
	public static String[] obtenerLista(String ruta) throws FileNotFoundException{
		
		String myLocalPath = ruta;
		
		Scanner scannerArchivo = new Scanner(new File(myLocalPath));
		
		scannerArchivo.useLocale(Locale.ENGLISH);
		
		scannerArchivo.useDelimiter(Pattern.compile("|"));
		
		List<String> listString = new ArrayList<String>();
		
		Integer bandera = 0;
		
		String palabra = "";
		
		String[] palabras = null;
		
		while (scannerArchivo.hasNextLine()) {
		    
			palabra = scannerArchivo.nextLine(); 
			
			palabras = palabra.split("[|]");
			
			while(bandera < palabras.length) {
				
				listString.add(palabras[bandera]);
				
				bandera ++;
			}
			
			bandera = 0;
		}
		
		scannerArchivo.close();
		
		return palabras;
	}
}