package com.overactive.selenium;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import junit.framework.Assert;

public class Test {

	public WebDriver driver = null;
	
	public void login() throws InterruptedException {
		
		App webApp = new App();
		
		driver = webApp.inicializar("chrome", "src/main/java/chromedriver.exe");
		
		webApp.instanciarPagina(driver, "http://adam.goucher.ca/parkcalc");
		
	}
	
	public void logout() {
		
		driver.close();
		
	}
	
	public void cargarPagina(String costo) throws FileNotFoundException, InterruptedException {
		
		LeerConScanner scan = new LeerConScanner();
		
		String ruta = "src/main/java/Archivo.csv";
		
		String[] palabras = scan.obtenerLista(ruta);
			
		Integer i = 0;
		
		//Selecciono el lote
		WebElement Lot = driver.findElement(By.name("Lot"));
		
		Lot.sendKeys(palabras[i]);
		
		i++;
		
		Thread.sleep(1000);
		
		//Selecciono la hora de entrada
		WebElement time1 = driver.findElement(By.name("EntryTime"));
		
		time1.clear();
		
		time1.sendKeys(palabras[i]);
		
		i++;
		
		Thread.sleep(1000);
		
		//Click en el radio buton Am
		WebElement radioAm = driver.findElement(By.xpath("/html/body/form/table/tbody/tr[2]/td[2]/font/input[2]"));
		
		radioAm.click();
		
		Thread.sleep(1000);
		
		//Selecciono dia de entrada
		WebElement entry = driver.findElement(By.name("EntryDate"));	
		
		entry.clear();
		
		entry.sendKeys(palabras[i]);
		
		i++;
		
		Thread.sleep(1000);
		
		//Selecciono la hora de Salida
		WebElement time2 = driver.findElement(By.name("ExitTime"));
		
		time2.clear();
		
		time2.sendKeys(palabras[i]);
		
		i++;
		
		Thread.sleep(1000);
		
		//Click en el radio buton Pm
		WebElement radioPm = driver.findElement(By.xpath("/html/body/form/table/tbody/tr[3]/td[2]/font/input[3]"));
		
		radioPm.click();
		
		Thread.sleep(1000);
		
		//Selecciono dia de Salida
		WebElement exit = driver.findElement(By.name("ExitDate"));	
		
		exit.clear();
		
		exit.sendKeys(palabras[i]);
		
		i++;
		
		Thread.sleep(1000);
		
		//Click en calcular
		WebElement submit = driver.findElement(By.name("Submit"));
		submit.click();
		Thread.sleep(2000);
		
		palabras = null;
		
		//Check del costo
		String check = driver.findElement(By.xpath("/html/body/form/table/tbody/tr[4]/td[2]/span[1]/font/b")).getText();
		
		Assert.assertEquals(costo, check);
	}
	
	@org.junit.Test
	public void calcular() throws InterruptedException, FileNotFoundException {
		
		login();
		
		Thread.sleep(5000);
		
		cargarPagina("$ 26.00");
		
		logout();
	}
	
}
