package com.overactive.selenium;

import static org.junit.Assert.*;
import static org.junit.Assume.assumeTrue;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import junit.framework.Assert;

public class TestStensul {

	public WebDriver driver = null;
	
	public WebDriver login() throws InterruptedException {
		
		App webApp = new App();
		
		driver = webApp.inicializar("chrome", "src/main/java/chromedriver.exe");
		
		webApp.instanciarPagina(driver, "http://immense-hollows-74271.herokuapp.com");
		
		return driver;
	}
	
	public void logout() {
		
		driver.close();
		
	}
	
	public void cargarPagina() throws IOException, InterruptedException {
		
		LeerConScanner scan = new LeerConScanner();
		
		String ruta = "src/main/java/ArchivoStensul.csv";
		
		String[] palabras = scan.obtenerLista(ruta);
			
		Integer i = 0,
				longitud = 0,
				contador = 0;
		
		//Se crea la variable archivo para crear el folder
		File file = new File(palabras[i]);
		i++;
		
		//Se crea el folder
	    if (!file.exists()) {
	    	if (file.mkdir()) {
	    		System.out.println("Directory is created!");
	    	} else {
	            System.out.println("Failed to create directory!");
	        }
	    }
	        
	    //Path original donde esta la imagen
		Path original = Paths.get(palabras[i]);
		i++;
		
		//Path a donde va la imagen
	    Path copied = Paths.get(palabras[i]);
		
	    //Se copia la imagen
		Files.copy(original, copied, StandardCopyOption.REPLACE_EXISTING);
		
		//Selecciono el campo de input de imagen
		WebElement inputImage = driver.findElement(By.id("inputImage"));
		
		inputImage.clear();
		Thread.sleep(1000);
		inputImage.sendKeys(palabras[i]);
		
		i++;
		
		Thread.sleep(1000);
		
		//Se ingresa el texto
		WebElement text = driver.findElement(By.name("text"));
		text.sendKeys(palabras[i]);
		
		Thread.sleep(1000);
		
		//click en save
		WebElement submit = driver.findElement(By.xpath("//*[@id=\"content\"]/div[2]/div/div/form/div[3]/button"));
		
		if(submit.isEnabled())
			submit.click();
		
		Thread.sleep(2000);
		

		WebElement autoOptions = driver.findElement(By.xpath("//*[@id=\"content\"]/div[1]/div/ul"));
		List<WebElement> optionsToSelect = autoOptions.findElements(By.tagName("li"));
		
		//Se toma la longitud de la lista
		longitud = optionsToSelect.size();
		
		//Se carga elemento a elemento en option para recorrer la lista de objetos
		//Y obtener el texto
		for(WebElement option : optionsToSelect){
			
			String word = option.getText();
			
			//Se parcea el texto
			String[] findWord = word.split("[\n]");
			
			//Se chequea si la frase obtenida es realmente la buscada
			if(findWord[2].equals(palabras[i]) && contador <= longitud){
				
				//Se crea una lista de archivos de la carpeta para eliminarlos
				String[] entries = file.list();
				
				//Se eliminan los archivos
				for(String s: entries){
				    File currentFile = new File(file.getPath(),s);
				    currentFile.delete();
				}
				
				//Se elimina la carpeta
				if (file.exists()){
					file.delete();
				}
				   				
				Assert.assertEquals(findWord[2], palabras[i]);
					
				break;
				
	        }else if(contador == longitud) {
	        	
	        	Assert.assertTrue(contador != longitud);
	        	
	        }
			
			contador++;
	    }
	}
	

	@org.junit.Test
	public void createItem() throws InterruptedException, IOException {
		
		driver = login();
		
		Thread.sleep(5000);
		
		cargarPagina();
		
		logout();
	}
	
	
}
