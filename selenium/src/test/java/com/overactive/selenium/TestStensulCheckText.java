package com.overactive.selenium;

import static org.junit.Assert.*;
import static org.junit.Assume.assumeTrue;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import junit.framework.Assert;

public class TestStensulCheckText {

	public WebDriver driver = null;

	public void findEspecificText() throws IOException, InterruptedException {
		
		LeerConScanner scan = new LeerConScanner();
		
		String ruta = "src/main/java/ArchivoStensul.csv";
		
		String[] palabras = scan.obtenerLista(ruta);
			
		Integer i = 0,
				longitud = 0,
				contador = 0;

		WebElement autoOptions = driver.findElement(By.xpath("//*[@id=\"content\"]/div[1]/div/ul"));
		List<WebElement> optionsToSelect = autoOptions.findElements(By.tagName("li"));
		
		//Se toma la longitud de la lista
		longitud = optionsToSelect.size();
		
		//Se carga elemento a elemento en option para recorrer la lista de objetos
		//Y obtener el texto
		for(WebElement option : optionsToSelect){
			
			String word = option.getText();
			
			//Se parcea el texto
			String[] findWord = word.split("[\n]");
			
			//Se chequea si la frase obtenida es realmente la buscada
			if(findWord[2].equals(palabras[6]) && contador <= longitud){
				   				
				Assert.assertEquals(findWord[2], palabras[6]);
					
				break;
				
	        }else if(contador == longitud) {
	        	
	        	Assert.assertTrue(contador != longitud);
	        	
	        }
			
			contador++;
	    }
	}
	

	@org.junit.Test
	public void checkText() throws InterruptedException, IOException {
		
		TestStensul stensul = new TestStensul();
		
		driver = stensul.login();
		
		Thread.sleep(5000);
		
		findEspecificText();
		
		stensul.logout();
	}
	
	
}
