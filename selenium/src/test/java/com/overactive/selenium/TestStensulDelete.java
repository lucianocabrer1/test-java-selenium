package com.overactive.selenium;

import static org.junit.Assert.*;
import static org.junit.Assume.assumeTrue;

import java.io.FileNotFoundException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import junit.framework.Assert;
import net.bytebuddy.asm.Advice.Exit;

public class TestStensulDelete {

	public WebDriver driver = null;
	
	@org.junit.Test
	public void delete() throws InterruptedException, FileNotFoundException {
		TestStensul stensul = new TestStensul();
		
		driver = stensul.login();
		
		LeerConScanner scan = new LeerConScanner();
		
		String ruta = "src/main/java/ArchivoStensul.csv";
		
		String[] palabras = scan.obtenerLista(ruta);
			
		Integer i = 0,
				contador = 1,
				longitud = 0;
		
		Thread.sleep(5000);
		
		WebElement autoOptions = driver.findElement(By.xpath("//*[@id=\"content\"]/div[1]/div/ul"));
		List<WebElement> optionsToSelect = autoOptions.findElements(By.tagName("li"));

		longitud = optionsToSelect.size();
		
		//Se carga elemento a elemento en option para recorrer la lista de objetos
		//Y obtener el texto
		for(WebElement option : optionsToSelect){
			
			String word = option.getText();
			String[] findWord = word.split("[\n]");
			
			//Se chequea si la frase obtenida es realmente la buscada
			if(findWord[2].equals(palabras[4]) && contador <= longitud) {
				
				//Se arma el path del boton Delete con la posicion de la lista
				String deleteClick = "//*[@id=\"content\"]/div[1]/div/ul/li[" + contador + "]/div/div/div[1]/button[2]";
	            
				WebElement delete = driver.findElement(By.xpath(deleteClick));
				
				if(delete.isEnabled()) {
					delete.click();
					
					Thread.sleep(1000);
					
					WebElement yes = driver.findElement(By.xpath("//*[@id=\"top\"]/div[4]/div/div/div[3]/button[1]"));
					
					Thread.sleep(1000);
					
					yes.click();
					
					Thread.sleep(1000);
					
					Boolean isPresent = driver.findElements(By.xpath(deleteClick)).size() > 0;
					Assert.assertTrue(isPresent == false);
				}
					
				break;
	        }else if(contador == longitud) {
	        	
	        	Assert.assertTrue(contador != longitud);
			}
			
			findWord = null;
			contador++;
	    }
		
		stensul.logout();
	}

	
}
