package com.overactive.selenium;

import static org.junit.Assert.*;
import static org.junit.Assume.assumeTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import junit.framework.Assert;
import net.bytebuddy.asm.Advice.Exit;

public class TestStensulEdit {

	public WebDriver driver = null;
	
	@org.junit.Test
	public void edit() throws InterruptedException, FileNotFoundException {
		TestStensul stensul = new TestStensul();
		
		driver = stensul.login();
		
		LeerConScanner scan = new LeerConScanner();
		
		String ruta = "src/main/java/ArchivoStensul.csv";
		
		String[] palabras = scan.obtenerLista(ruta);
			
		Integer i = 0,
				contador = 1,
				longitud = 0;
		
		Thread.sleep(5000);
		
		WebElement autoOptions = driver.findElement(By.xpath("//*[@id=\"content\"]/div[1]/div/ul"));
		List<WebElement> optionsToSelect = autoOptions.findElements(By.tagName("li"));

		longitud = optionsToSelect.size();
		
		//Se carga elemento a elemento en option para recorrer la lista de objetos
		//Y obtener el texto
		for(WebElement option : optionsToSelect){
			
			String word = option.getText();
			String[] findWord = word.split("[\n]");
			
			//Se chequea si la frase obtenida es realmente la buscada
			if(findWord[2].equals(palabras[3]) && contador <= longitud) {
				
				//Se arma el path del boton Delete con la posicion de la lista
				String editClick = "//*[@id=\"content\"]/div[1]/div/ul/li[" + contador + "]/div/div/div[1]/button[1]";
	            
				WebElement edit = driver.findElement(By.xpath(editClick));
				
				if(edit.isEnabled()) {
					edit.click();
					
					Thread.sleep(1000);
					
					//Se ingresa el texto
					WebElement text = driver.findElement(By.name("text"));
					text.clear();
					text.sendKeys(palabras[4]);
					
					Thread.sleep(1000);
					
					//click en save
					WebElement submit = driver.findElement(By.xpath("//*[@id=\"content\"]/div[2]/div/div/form/div[3]/button[2]"));
					
					if(submit.isEnabled())
						submit.click();
					
					Thread.sleep(2000);
					
					break;
				}
					
	        }else if(contador == longitud) {
	        	
	        	Assert.assertTrue(contador != longitud);
			}
			
			findWord = null;
			contador++;
	    }
		
		contador = 0;
		longitud = 0;
		
		WebElement autoOptions2 = driver.findElement(By.xpath("//*[@id=\"content\"]/div[1]/div/ul"));
		List<WebElement> optionsToSelect2 = autoOptions.findElements(By.tagName("li"));
		
		//Se toma la longitud de la lista
		longitud = optionsToSelect.size();
		
		//Se carga elemento a elemento en option para recorrer la lista de objetos
		//Y obtener el texto
		for(WebElement option2 : optionsToSelect2){
			
			String word2 = option2.getText();
			
			//Se parcea el texto
			String[] findWord2 = word2.split("[\n]");
			
			//Se chequea si la frase obtenida es realmente la buscada
			if(findWord2[2].equals(palabras[4]) && contador <= longitud){
				   				
				Assert.assertEquals(findWord2[2], palabras[4]);
					
				break;
				
	        }else if(contador == longitud) {
	        	
	        	Assert.assertTrue(contador != longitud);
	        	
	        }
			
			contador++;
	    }
		
		stensul.logout();
	}

	
}
